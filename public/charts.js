$(function() {
  $.ajax({
     type: "GET",
     url: "session_history.csv",
     dataType: "text",
     success: function(data) { parseCSV(data) }
  });
});

function parseCSV(data) {
  var results = Papa.parse(data, { delimeter: ',' });
  var info = {}, passed = [], failed = [], duration = [];
  $.each( getData(results), function( key, value ) {
    var dateKey = getDate(value[2])
    if ( info[dateKey] ) {
      info[dateKey]['passed'] += parseInt(value[11])
      info[dateKey]['failed'] += parseInt(value[12])
      info[dateKey]['duration'] += parseInt(value[4])
    } else {
      info[dateKey] = { passed: parseInt(value[11]),
                        failed : parseInt(value[12]),
                        duration: parseInt(value[4]) }
    }
  });

  $.each( info, function( key, value ) {
    passed.push(value['passed']);
    failed.push(value['failed']);
    duration.push(value['duration'])
  });

  drawFailedPassedChart({ info: info, passed: passed, failed: failed });
  drawDurationChart({ info: info, duration: duration });
}

function getData(results) {
  return results.data.slice(1, -1)
}

function getDate(date) {
  return new Date(date).toLocaleDateString()
}

function sumFn(a, b) {
  return a + b;
}

function drawFailedPassedChart(data) {
  const THRESHOLD = 0.05;
  var keys = Object.keys(data['info']), passed = data['passed'],
      failed = data['failed'],
      maxValue = Math.max(...failed), lowerLine = Math.ceil(THRESHOLD * maxValue);


  var chart = Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Tests failed/passed statistic'
      },
      xAxis: {
          categories: keys,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Tests amount'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              dataLabels: {
                  enabled: true,
                  color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
              },
          },
      },
      series: [{
          name: 'Passed',
          data: passed,
          color: '#1db34f',
      },{
        name: 'Failed',
        data: failed,
        color: '#f17f49'
      }]
  });

  $.each( failed, function( key, value ) {
    if (value >= lowerLine) {
      chart.series[1].data[key].update({ dataLabels: { color: 'red' }});
    }
  });
}

function drawDurationChart(data) {
  var keys = Object.keys(data['info']), duration = data['duration']
  Highcharts.chart('container-duration', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Durations(seconds) per day'
      },
      xAxis: {
          categories: keys,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Duration (seconds)'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              dataLabels: {
                  enabled: true,
                  color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'

              }
          }
      },
      series: [{
          name: 'Duration',
          data: duration,
          color: 'blue'
      }]
  });
}
