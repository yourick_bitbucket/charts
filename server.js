var http = require("http");
var fs = require("fs");
var path = require("path");
var mime = require("mime");
var papa = require("papaparse");
var cache = {};
function send404(responce){
    responce.writeHead(404,{"content-type":"text/plain"});
    responce.write("Error 404: resourse not found");
    responce.end()
}
function sendFile(responce,filePath,fileContents){
    responce.writeHead(200,{"content-type": mime.lookup(path.basename(filePath))});
    responce.end(fileContents);
}
function serveStatic(responce,cache,abspath){
    if(cache[abspath]){
        sendFile(responce,abspath,cache[abspath]);
    }else{
        fs.exists(abspath,function(exists){
            if(exists){
                fs.readFile(abspath,function(err,data){
                    if(err){
                        send404(responce);
                    }else{
                        cache[abspath] = data;
                        sendFile(responce,abspath,data);
                    }
                })
            }else{
                send404(responce)
            }
        })
    }
}
var server = http.createServer(function(request,responce){
    var filePath = false;
    if(request.url == '/'){
        filePath = 'public/index.html';
    }else{
        filePath = 'public'+request.url;
    }
    var abspath = './'+filePath;
    serveStatic(responce,cache,abspath);
})
server.listen(8080,function(){
    console.log("server running on 8080");
})
